## convert UM tile polylines to polygons and remove overlaps

## clear workspace
rm(list = ls())

## load packages
library(raster)
library(rgeos)
library(rgdal)
library(maptools)

# set yr
yr <- 2007

# Set paths
um_tile_path <- paste('X:/DB/ERS/UM',yr,sep='') 
shp_path <- 'X:/DB/SHP'
shp_path_raster <- 'X:/DB/ERS/UM_MB_RASTER'
batch_path <- 'X:/DB/BATCH'

setwd(shp_path)

## get tile ID 
tile_name <- read.csv('tile_id.csv', header = T) ## CSV file containing list of tile IDs

## read in polylines file
in_tiles <- shapefile('UM2009_IndexPolyline.shp', stringsAsFactors = F)
plot(in_tiles) 

## convert polylines to polygon
in_poly <- gPolygonize(in_tiles)

## create spatial polygon ID
ID <- data.frame(seq(1, length(in_poly), 1))
names(ID)[1] <- "ID"

## make spatial polygons spatial polygon df
in_poly_df <- SpatialPolygonsDataFrame(in_poly, ID)

## loop over each polygon and remove overlapping areas

## first get initial polygon - then grow this SPDF
poly_out <- in_poly_df[1, ]

for (i in 2:length(in_poly)) {
  
  ##status
  print(i)
  
  ## polygons less than than polygon of interest - id polygon sacrifices to this group
  poly_lt <- in_poly_df[1:(i-1),]
  poly_lt$merge_id <- 1 ## dissolve on this id
  poly_lt <- gUnaryUnion(poly_lt, id = poly_lt@data$merge_id) ## dissolve
  
  ## id polygon 
  poly <- in_poly_df[i, ] 
  
  ## remove overlap with less than group
  poly_diff <- poly - poly_lt
  
  poly_out <- rbind(poly_out, poly_diff)
  
}

## drop final row of tiles to match UM true extent
plot(poly_out)
poly_out <- subset(poly_out, ID < 73 | ID == 76) ## UM polylines file contains 76 tiles where as UM has 73

row_names <- c(1:72, 76)
tile_name <- data.frame(tile_name, row.names = row_names)

## Add tile names to tiles
poly_out_df <- SpatialPolygonsDataFrame(poly_out, tile_name)
plot(poly_out_df)

## Make polygon file with overlaps
in_poly_df <- subset(in_poly_df, ID < 73 | ID == 76) ## UM polylines file contains 76 tiles where as UM has 73

## Add tile names to tiles
in_poly_df <- SpatialPolygonsDataFrame(in_poly_df, tile_name)
plot(in_poly_df)

## Read out polygon file of tiles with no overlaps
shapefile(in_poly_df, 'UM2009_IndexPolygons.shp', overwrite = T)

## Read out polygon file of tiles with no overlaps
shapefile(poly_out_df, 'UM2009_IndexPolygons_no_overlaps.shp', overwrite = T)
