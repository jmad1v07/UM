/*
Compute monthly MODIS LST based temperature metrics per ~1 km grid cell across the Perth Peel region. 
Also compute Landsat based NDVI metrics per ~1 km grid cell and monthly precipitation per 1 km grid cell.

Variables:

lst_acc_day = monthly sum of daily LST values
n_obs_day = number of possible observations per month
n_days_day = number of good quality observations per month (divide lst_acc_day by this to get monthly average LST)
lst_acc_night = monthly sum of nighttime LST values
n_days_night = number of good quality observations per month (nighttime obs - divide lst_acc_night by this to get monthly average nighttime LST)
ndvi_max = monthly max NDVI from Landsat
rainfall = monthly precip (mm) from CHIRPS dataset
*/ 

// SET UP INITIAL VARIABLES AND FUNCTIONS //

// Make Perth Peel region a geometry object
var location = perth_peel.geometry();

/*
Function to get apply MODIS QA data to daily LST observations, convert from Kelvin to deg C, 
and return daily LST and an extreme degree day metric of days of exposure above 30 deg C (30 deg C  = 1 degree day)
*/ 
var applyQA_lst = function(input){
  // day time LST  
  var qa = input.select('QC_Day');
  var qa1 = qa.eq(0).or(qa.eq(5)).or(qa.eq(17)).or(qa.eq(21)).or(qa.eq(33)).or(qa.eq(37)).or(qa.eq(49)).or(qa.eq(53)); // LST with < 1 kelvin error
  var lst = input.select('LST_Day_1km').multiply(0.02);  // rescale LST in Kelvin
  var lst = lst.subtract(273.15); // Kelvin to deg C
  var lst = lst.updateMask(qa1);
  var lst1 = lst; // lst1 is daily lst
  var day = ee.Image.constant(1);
  var lst = lst1.addBands(day).addBands(qa1);
  
  
  // night time LST
  var qa = input.select('QC_Night');
  var qa2 = qa.eq(0).or(qa.eq(5)).or(qa.eq(17)).or(qa.eq(21)).or(qa.eq(33)).or(qa.eq(37)).or(qa.eq(49)).or(qa.eq(53)); // LST with < 1 kelvin error
  var lstn = input.select('LST_Night_1km').multiply(0.02);  // rescale LST in Kelvin
  var lstn = lstn.subtract(273.15); // Kelvin to deg C
  var lstn = lstn.updateMask(qa2);
  var lst2 = lstn; // lst2 is nighttime LST
  var night = ee.Image.constant(1);
  var lst = lst.addBands(lst2).addBands(night).addBands(qa2);// convert to extreme degree days
  return lst;
};

// Function to get 8 day composite MODIS LST product
var apply_8_QA_lst = function(input){
  // day time LST  
  var lst = input.select('LST_Day_1km').multiply(0.02);  // rescale LST in Kelvin
  var lst = lst.subtract(273.15); // Kelvin to deg C
  var lst1 = lst;
  var day = ee.Image.constant(1);
  var lst = lst1.addBands(day);
  
  
  // night time LST
  var lstn = input.select('LST_Night_1km').multiply(0.02);  // rescale LST in Kelvin
  var lstn = lstn.subtract(273.15); // Kelvin to deg C
  var lst2 = lstn;
  var night = ee.Image.constant(1);
  var lst = lst.addBands(lst2).addBands(night);
  return lst;
};

// LANDSAT FUNCTIONS

/* Some Functions */

// Functions to get Landsat Data
function getlandsat5(start_date, end_date, location){
  var l5_l1t = ee.ImageCollection("LANDSAT/LT5_SR")
  .filterDate(start_date, end_date)
  .filterBounds(location); 
  return l5_l1t;
}

function getlandsat7(start_date, end_date, location){
  var l7_l1t = ee.ImageCollection("LANDSAT/LE7_SR")
  .filterDate(start_date, end_date)
  .filterBounds(location); 
  return l7_l1t;
}

function getlandsat8(start_date, end_date, location){
  var l8_l1t = ee.ImageCollection('LANDSAT/LC8_SR')
  .filterDate(start_date, end_date)
  .filterBounds(location); 
  return l8_l1t;
}

//Function to make NDVI band for each Landsat observation and apply cloud mask using cfmask == 4
var cloud_ndvi_day_l5 = function(img){
var cloud = img.select('cfmask').neq(4);
var ndvi = img.normalizedDifference(['B4', 'B3']);
var start = img.get('system:time_start');  
var end = img.get('system:time_end');
var day = ee.Image.constant(img.date().getRelative('day', 'year').add(0))
var output = ndvi;
var output = output.set({'system:time_start': start});
var output = output.set({'system:time_end': end});  
var output = output.addBands(day.rename('DAY').int());
var output = output.mask(cloud);
return output;
};

var cloud_ndvi_day_l7 = function(img){
var cloud = img.select('cfmask').neq(4);
var ndvi = img.normalizedDifference(['B4', 'B3']);
var start = img.get('system:time_start');  
var end = img.get('system:time_end');
var day = ee.Image.constant(img.date().getRelative('day', 'year').add(0))
var output = ndvi;
var output = output.set({'system:time_start': start});
var output = output.set({'system:time_end': end});  
var output = output.addBands(day.rename('DAY').int());
var output = output.mask(cloud);
return output;
};

var cloud_ndvi_day_l8 = function(img){
var cloud = img.select('cfmask').neq(4);
var ndvi = img.normalizedDifference(['B5', 'B4']);
var start = img.get('system:time_start');  
var end = img.get('system:time_end');
var day = ee.Image.constant(img.date().getRelative('day', 'year').add(0))
var output = ndvi;
var output = output.set({'system:time_start': start});
var output = output.set({'system:time_end': end});  
var output = output.addBands(day.rename('DAY').int());
var output = output.mask(cloud);
return output;
};

////////////////////////////////////////
// NOW APPLY THE FUNCTIONS
////////////////////////////////////////

//loop through years and export files
for (var idx = 2003; idx <= 2016; idx++){

var year1 = idx;
// loop through months - this is to read out multiband rasters with month-year combinations for all computed variables
for (var mdx = 1; mdx <= 12; mdx++){

var start_date = ee.Date.fromYMD(idx,1,1); 
var end_date = ee.Date.fromYMD(idx,12,31);


var day_lst = ee.ImageCollection('MODIS/MYD11A1') 
 .filterDate(start_date, end_date)
 .filter(ee.Filter.calendarRange(mdx, mdx, 'month'))
 .select(['LST_Day_1km', 'QC_Day', 'LST_Night_1km', 'QC_Night'])
 .filterBounds(location);

var day_8_lst = ee.ImageCollection('MODIS/MYD11A2') 
 .filterDate(start_date, end_date)
 .filter(ee.Filter.calendarRange(mdx, mdx, 'month'))
 .select(['LST_Day_1km', 'QC_Day', 'LST_Night_1km', 'QC_Night'])
 .filterBounds(location);
 //print(day_8_lst);

var lst_out = day_lst.map(applyQA_lst);
var lst_8_out = day_8_lst.map(apply_8_QA_lst);
//Map.addLayer(lst_8_out);

// Get sum of LST EDD
var lst_collapse = lst_out.sum();
var lst_collapse = lst_collapse.rename('acc_lst_day', 'n_obs_day', 'n_days_day', 'acc_lst_night', 'n_obs_night', 'n_days_night');
var lst_collapse = lst_collapse.float();

var lst_8_collapse = lst_8_out.sum();
var lst_8_collapse = lst_8_collapse.rename('acc_lst_day', 'n_obs_day', 'acc_lst_night', 'n_obs_night');
var lst_8_collapse = lst_8_collapse.float();

// Apply Landsat Functions
//Get all Landsat 5, 7, 8 data overlapping time period and location
var l5_l1t = getlandsat5(start_date, end_date, location);
print(l5_l1t);

var l7_l1t = getlandsat7(start_date, end_date, location);
print(l7_l1t);

var l8_l1t = getlandsat8(start_date, end_date, location);
print(l8_l1t);

// add NDVI band and apply cloud masks
var l5 = l5_l1t.map(cloud_ndvi_day_l5);
print(l5);

var l7 = l7_l1t.map(cloud_ndvi_day_l7);
print(l7);

var l8 = l8_l1t.map(cloud_ndvi_day_l8);
print(l8);

// Merge Landsat 5, 7, 8 image collections and sort by time
var lm1 = ee.ImageCollection(l8.merge(l7));
var lm2 = ee.ImageCollection(lm1.merge(l5)).sort('system:time_start');
print(lm2);

// Get Landsat peak NDVI, day of peak NDVI, and no. of observations
var ndvi_max = lm2.qualityMosaic('nd').rename('ndvi_max');

var lst_collapse = lst_collapse.addBands(ndvi_max);
var lst_collapse = lst_collapse.clip(location);

var lst_8_collapse = lst_8_collapse.addBands(ndvi_max);
var lst_8_collapse = lst_8_collapse.clip(location);


// GET PRECIPITATION DATA //
//Filter the Image Collection to Desired Date Range:    
var chirps = ee.ImageCollection('UCSB-CHG/CHIRPS/PENTAD')
  .filterDate(start_date, end_date)
  .filter(ee.Filter.calendarRange(mdx, mdx, 'month'));

//Calculate the Sum of Precipitation Values:
var rainfall = chirps.sum();
var rainfall = rainfall.clip(location);
var rainfall = rainfall.float();
var lst_collapse = lst_collapse.addBands(rainfall);
var lst_8_collapse = lst_8_collapse.addBands(rainfall);

//print(lst_collapse);
//Map.addLayer(lst_collapse);

Export.image.toDrive({
  image: lst_collapse,
  description: 'perth_lst_'+mdx+'_'+idx,
  scale: 1000,
  region:location
});

Export.image.toDrive({
  image: lst_8_collapse,
  description: 'perth_8_lst_'+mdx+'_'+idx,
  scale: 1000,
  region:location
});

}
}