# UM

## Overview

This a live repo for analysis performed by the [CAUL hub] (https://www.nespurban.edu.au/) at the University of Western Australia .

![](/caul_hub.jpg)

### Urban Monitor Data

Code to process [Urban Monitor](http://www.environment.gov.au/system/files/resources/23952ac8-31d4-44b0-bad6-3a4179f4e3bb/files/urban-monitor-final-report.pdf) vegetation height data collected by CSIRO for the Perth and Peel regions (Australia) to compute summary statistics for administrative units. 

### Google Earth Engine 

This repo also contains scripts that can be executed in Google Earth Engine to generate land surface temperature and vegetation metrics using MODIS and Landsat satellite data over Perth. 

### Code / Software Requirements

Code is written in Microsoft R Open 3.4.1 and JavaScript (for Google Earth Engine scripts). Some R scripts generate .bat files to execute 7-Zip or GDAL commands from the command line. To execute scripts in Google Earth Engine requires signing up for an account. 

## Software:

- Microsoft R Open 3.4.1
- GDAL 
- 7-Zip

## R libraries:
Several R packages are required to replicate this workflow.

To install these packages run the following code:
```
install.packages(c('raster', 'rgeos', 'rgdal', 'dplyr', 'lfe', 'plm', 'car', 'splm', 'spdep', 'sp', 'lattice', 'colorRamps', 'RColorBrewer', 'ggplot2', 'GWmodel', 'randomForest', 'reprtree'))
```

### Workflow

PreProc_gzUnzip.R - generates a batch file to use 7zip to unzip .gz Urban Monitor tiled vegetation height data. 

PreProc_CopyErs.R - copies the .ERS header files from the archive filestore where Urban Monitor data is stored to working filestore.

PreProc_RenameFiles.R - rename the .ERS and header files to shorter UM tile filenames.

PreProc_RemoveUMTileOverlaps.R - remove overlaps between the outlines of the UM tiles and create shapefile boundary for each UM tile (with and without overlaps).

PreProc_RasterizeShapefile.R - generic script to rasterize shapefiles (via batch script calling GDAL) within each of the UM tiles.

PreProc_PolygonizeModisPixels.R - create polygon outlines for each ~1 km MODIS pixel from the MYD11A2 product.

PreProc_RasterizeModisPixels.R - rasterize modis pixels by unique pixel ID to the UM spatial resolution.

Analysis_ZonalStatsTemplate.R - compute zonal stats of UM vegetation height data by UM tile.

Analysis_CombineTileZStats.R - combine zonal stats computed per UM tile to one file.

DataPrep_Analysis_ModisLST.R - combine Landsat NDVI, MODIS LST, and UM vegetation height data into one dataset, perform data cleaning and pre-processing, exploratory data analysis, and statistical analysis linking vegetation height data and urban land surface temperature. 

## Folder structure

To replicate the analysis here you will need folder structure to mimic the setup below. 

X drive = working folder

'X:/DB/ERS' - where the UM vegetation height and rasterized shapefiles are stored. See R scripts for relevant sub-folders. 

'X:/DB/SHP' - where vector data is stored.

'X:/DB/BATCH' - where batch files that run GDAL / 7zip commands off the command line are stored. 

Y drive = archive

'Y:/UM'

use the Y drive to store raw data as received from CSIRO



