## Shapefile of MODIS pixels with Unique MODIS pixel ID

rm(list = ls())

library(raster)
library(rgeos)
library(rgdal)

modis_path <- 'X:/DB/RS_DATA'
shp_path <- 'X:/DB/SHP'
setwd(modis_path)

## Get MODIS pixel raster

outline_rast <- raster('perth_lst_12004.tif')
ext <- extent(outline_rast)
unique_vals <- 1:ncell(outline_rast)
mat <- matrix(unique_vals, nrow(outline_rast), ncol(outline_rast))
new_rast <- raster(mat, xmn = ext[1], xmx = ext[2], ymn = ext[3], ymx = ext[4], crs = '+proj=longlat +datum=WGS84 +no_defs +ellps=WGS84 +towgs84=0,0,0')

## convert to Polygon
new_poly <- rasterToPolygons(new_rast, fun=NULL, n=4, na.rm=TRUE, digits=12, dissolve=FALSE)

## convert to same proj as urban monitor
new_poly = spTransform(new_poly, CRS("+proj=utm +zone=50 +south +ellps=GRS80 +units=m +no_defs"))
names(new_poly@data)[1] <- 'modis_id'
plot(new_poly)

## clip to Perth Peel outline
pp_shp_ll <- shapefile(paste(shp_path,'/perth_peel_latlon.shp', sep=''))
pp_shp_utm <- spTransform(pp_shp_ll, CRS("+proj=utm +zone=50 +south +ellps=GRS80 +units=m +no_defs"))

## clip MODIS outlines by Perth Peel extent
new_poly_pp <- raster::intersect(new_poly, pp_shp_utm) 

## save file
shapefile(new_poly_pp, paste(shp_path, '/modis_pixel_outline.shp', sep=''), overwrite = T)
